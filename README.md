# aspnet-docker
Simple demo of using GitLab pipeline to deploy a .net core 3 project on to AWS Kubernetes service(EKS). 

## Requirements
In order to run this example, you'll need to:
* Have an EKS cluster set up on AWS service. 
* fork this project or create anther project and pushed all the code from this project to your project.
* Have a Gitlab runner set up for your project.

## File Structure
```
./aspnet-docker/
├── .git
├── .gitignore
├── .gitlab-ci.yml
├── .kubernetes
├── .vs
├── LICENSE
├── Pro690P2APP.sln
├── Pro690P2App
├── README.md
└── XUnitTestProject1
```

### Pro690P2App
This is the demo application written in asp.net core 3. There is also a Docker file in the folder so you can use it to build am image out of the application.

### XUnitTestProject1
This is the test project used to test the main application. You could run it by using the "dotnet run test" command after you download all the source code and cd into the XUnitTestProject1 folder.

### .kubernetes
This is the kubernetes folder. There are two files in it. The services.yaml is used to deploy the service on to the EKS. And deployment.yaml is used to deploy the image from the GitLab registry to EKS cluster.

### .gitlab-ci.yml
The .gitlab-ci.yml file is a YAML file where you configure specific instructions for GitLab CI/CD. When you committed your changes, a pipeline started. In this project, there are 5 stages/jobs listed:
* run test
* docker build
* docker push
* deploy services
* deploy app
Each job will run after the previous one. And if any one fails, you will get a notice from GitLab.

## Variables you need to set up
There are some environment variables you need to find before you use this pipeline. 
Go to **Setting -> CI/CD -> Variables **:
* AWS_ACCESS_KEY_ID : Your AWS access key id, you can get it from your aws service console
* AWS_DEFAULT_REGION: Your AWS EKS cluster's region
* AWS_SECRET_ACCESS_KEY: Your aws secrete access key, you can get it from your aws service console
* KUBECONFIG: The config file you use to connect to your EKS cluster. You can get it using AWS CLI

## Running the project
The steps to run are:
1. Fork the project
2. Set up the gitlab runner
3. Set up all the variables
4. Start your EKS cluster and set up enough nodes.
5. Make a change to the source code and commit.
6. You should be able to see the pipeline working and new version of application will deployed on your EKS cluster if all jobs succeed. 

## Contact
* jyu205@myseneca.ca
* mokimoto@myseneca.ca
* szeng9@myseneca.ca

## Thanks!
